package com.example.counterapp

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.widget.Toast

class BoundService : Service() {

    private val binder = BoundBinder()
    private var cntValue = 0

    inner class BoundBinder : Binder(){
        fun getService() = this@BoundService
    }

    override fun onBind(p0: Intent?): IBinder? {
        return binder
    }

    fun incrementVal(){
        cntValue++
    }

    fun decrementVal(){
        if(cntValue > 0)
            cntValue--
    }

    fun currentVal(): Int {
        return cntValue
    }
    fun showVal(){
        Toast.makeText(this, "$cntValue", Toast.LENGTH_SHORT).show()
    }

}